# Sobki Profile DSFR

This profile is based on
[UI Suite DSFR](https://www.drupal.org/project/ui_suite_dsfr) and its
goal is to provide a clean and ready to use DSFR integration.

See [Sobki's documentation](https://project.pages.drupalcode.org/sobki)
for general and common documentation of the Sobki ecosystem.

There is currently no specific documentation for this profile.


## Requirements

This profile has no special system requirements. See
[Sobki's documentation](https://project.pages.drupalcode.org/sobki/installation/requirements)

This profile requires Composer to gather its dependencies. See
[Sobki's documentation](https://project.pages.drupalcode.org/sobki/installation/composer)

In addition to the Composer specificities common to all Sobki's profiles,
the following content should be present into the project `composer.json`:

```json
{
  "extra": {
    ...
    "installer-paths": {
      "app/libraries/dsfr": [
        "npm-asset/gouvfr--dsfr"
      ],
      ...
      "app/libraries/{$name}": [
        "type:drupal-library",
        "type:bower-asset",
        "type:npm-asset"
      ]
    }
  }
}
```


## Installation

Install as you would normally install a contributed Drupal profile.


## Configuration

After the installation, adapt the provided configuration to your needs.
