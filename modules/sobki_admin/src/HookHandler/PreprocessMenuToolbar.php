<?php

declare(strict_types=1);

namespace Drupal\sobki_admin\HookHandler;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\gin\GinSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allow to use theme's default logo in Gin's toolbar.
 */
class PreprocessMenuToolbar implements ContainerInjectionInterface {

  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected ClassResolverInterface $classResolver,
    protected string $appRoot,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('module_handler'),
      $container->get('class_resolver'),
      $container->getParameter('app.root'),
    );
  }

  /**
   * Allow to use theme's default logo in Gin's toolbar.
   *
   * @param array $variables
   *   The preprocessed variables.
   */
  public function preprocess(array &$variables): void {
    if (!$this->moduleHandler->moduleExists('gin_toolbar')) {
      return;
    }

    // Are we relevant?
    if (!\_gin_toolbar_gin_is_active()) {
      return;
    }

    /** @var \Drupal\gin\GinSettings $settings */
    $settings = $this->classResolver->getInstanceFromDefinition(GinSettings::class);

    $logo_url = $settings->getDefault('logo.url');
    if (\is_file($this->appRoot . $logo_url)) {
      $variables['icon_path'] = $logo_url;
      // Force icon_default to FALSE due to logic in Gin Toolbar template.
      $variables['icon_default'] = FALSE;
    }
  }

}
