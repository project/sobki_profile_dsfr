<?php

declare(strict_types=1);

namespace Drupal\sobki_admin\HookHandler;

use Drupal\Core\Form\FormStateInterface;

/**
 * Layout Builder block form alter.
 */
class FormLayoutBuilderBlockAlter {

  /**
   * Customize block config form.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public function formAlter(array &$form, FormStateInterface $formState): void {
    // Hide "Block description" because the route title is already overridden to
    // display it.
    if (isset($form['settings']['admin_label'])) {
      $form['settings']['admin_label']['#access'] = FALSE;
    }

    // Add a separator between "block title" section and "block content"
    // section.
    if (isset($form['settings']['label_display'])) {
      $form['settings']['label_display']['#suffix'] = '<hr>';
    }
  }

}
