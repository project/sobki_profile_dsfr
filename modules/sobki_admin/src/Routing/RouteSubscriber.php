<?php

declare(strict_types=1);

namespace Drupal\sobki_admin\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $route = $collection->get('layout_builder.add_block');
    if ($route) {
      $route->setDefault('_title_callback', 'sobki_admin.helper:getAddBlockRouteTitle');
    }
    $route = $collection->get('layout_builder.update_block');
    if ($route) {
      $route->setDefault('_title_callback', 'sobki_admin.helper:getUpdateBlockRouteTitle');
    }
  }

}
