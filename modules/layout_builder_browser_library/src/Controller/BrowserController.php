<?php

declare(strict_types=1);

namespace Drupal\layout_builder_browser_library\Controller;

use Drupal\Core\Url;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_browser\Controller\BrowserController as BrowserControllerBase;
use Drupal\layout_builder_browser\Entity\LayoutBuilderBrowserBlock;

/**
 * Controller class responsible for managing browser functionality.
 */
class BrowserController extends BrowserControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getBlocks(SectionStorageInterface $section_storage, $delta, $region, array $blocks): array {
    $links = [];

    foreach ($blocks as $block_id => $block) {
      $attributes = $this->getAjaxAttributes();
      $attributes['class'][] = 'js-layout-builder-block-link';

      $block_render_array = [];
      // Use block image when available.
      if (!empty($block['layout_builder_browser_data']) && isset($block['layout_builder_browser_data']->image_path) && \trim($block['layout_builder_browser_data']->image_path) != '') {
        $block_render_array['image'] = [
          '#theme' => 'image',
          '#uri' => $block['layout_builder_browser_data']->image_path,
          '#alt' => $block['layout_builder_browser_data']->image_alt ?? '',
        ];
      }
      // Use category image when available.
      elseif (!empty($block['layout_builder_browser_category_data']) && isset($block['layout_builder_browser_category_data']->image_path) && \trim($block['layout_builder_browser_category_data']->image_path) != '') {
        $block_render_array['image'] = [
          '#theme' => 'image',
          '#uri' => $block['layout_builder_browser_category_data']->image_path,
          '#alt' => $block['layout_builder_browser_category_data']->image_alt ?? '',
        ];
      }
      $block_render_array['label'] = ['#markup' => (empty($block['layout_builder_browser_data'])) ? $block['admin_label'] : $block['layout_builder_browser_data']->label()];
      $link = [
        '#type' => 'link',
        '#title' => $block_render_array,
        '#url' => Url::fromRoute(
          'layout_builder.add_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $block_id,
          ]
        ),
        '#attributes' => $attributes,
      ];

      $blockRender = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'layout-builder-browser-block-item',
          ],
        ],
        'link' => $link,
      ];

      // Tooltip.
      if (isset($block['layout_builder_browser_data']) && $block['layout_builder_browser_data'] instanceof LayoutBuilderBrowserBlock) {
        $layoutBuilderBlock = $block['layout_builder_browser_data'];
        $settings = $layoutBuilderBlock->getThirdPartySettings('layout_builder_browser_library');
        if (isset($settings['library_image_path']) && \trim($settings['library_image_path']) != '') {
          $tooltip_link = $link;
          $tooltip_link['#title'] = [
            '#theme' => 'image',
            '#uri' => $settings['library_image_path'],
            '#alt' => $settings['library_image_alt'] ?? '',
          ];
          $blockRender['tooltip'] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['lb-tooltip'],
            ],
            'image' => $tooltip_link,
            '#attached' => [
              'library' => [
                'layout_builder_browser_library/tooltip',
              ],
            ],
          ];
        }
      }

      $links[] = $blockRender;
    }
    return $links;
  }

}
