<?php

declare(strict_types=1);

namespace Drupal\sobki_user\Permissions;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides permissions not-existing yet during installation.
 */
class RoleDelegation {

  use StringTranslationTrait;

  /**
   * Get permissions for role_delegation during installation.
   *
   * This is necessary because of a circular dependency between roles and
   * non-existing permissions during installation.
   *
   * @return array
   *   Permissions array.
   *
   * @see https://www.drupal.org/project/role_delegation/issues/3354012
   */
  public function permissions(): array {
    $permissions = [];

    // @todo update roles to match Sobki Bootstrap.
    $roles = [
      'content_editor' => $this->t('Content editor'),
      'webmaster' => $this->t('Webmaster'),
    ];

    foreach ($roles as $rid => $role) {
      $permissions[\sprintf('assign %s role', $rid)] = [
        'title' => $this->t('Assign %role role', ['%role' => $role]),
        'provider' => 'role_delegation',
      ];
    }

    return $permissions;
  }

}
